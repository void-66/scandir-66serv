# Changelog for scandir-66serv

---

# In 0.2.1

- Convenient release to udpate dependencies:
	- 66 v0.5.1.0
	- 66-tools v0.0.6.2
	- optional dependency:
		- dbus-66serv 0.2.0 for environment configuration

---

# In 0.2.0

- Adapt to 66 v0.4.0.1

---

# In 0.1.0
	
- first commit
