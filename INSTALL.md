Build Instructions
------------------

## Requirements

- GNU make version 3.81 or later
  
This software will install on any operating system that implements POSIX.1-2008, available at [opengroup](http://pubs.opengroup.org/onlinepubs/9699919799/).

## Standard usage

`./configure && make && sudo make install` will work for most users.

## Customization

You can customize paths via flags given to configure. See `./configure --help` for a list of all available configure options.

## Runtime dependencies
  
- execline version 2.6.0.2 or later: http://skarnet.org/software/execline/
- s6 version 2.9.1.0 or later: http://skarnet.org/software/s6/
- 66 version 0.5.1.0 or later https://framagit.org/Obarun/66/
- 66-tools version 0.0.6.2 or later https://framagit.org/Obarun/66-tools/
- optional dependencies:
  * dbus-66serv version 0.2.0 or later https://framagit.org/pkg/observice/dbus-66serv
